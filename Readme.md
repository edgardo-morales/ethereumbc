## Recurso de BlockChain
[NFTs](https://www.youtube.com/watch?v=YKRpRmnIN_g)

- https://medium.com/opensea
- https://district0x.io/
- https://www.coingecko.com/en/nft
- https://www.okex.com/join/1850629
- https://ascendex.com/
- https://www.bitforex.com/
- https://v2.info.uniswap.org/
- https://token.veve.me/
- https://exberry.io/

# Recursos NFT
- https://wyvernprotocol.com/
- https://superfarm.com/
- https://on.wax.io/
## Cadenas de Bloques
- https://www.onflow.org/post/viv3-nft-marketplace-to-empower-artists-and-creators

## MarketPlace
- https://viv3.com
- https://opensea.io/
- https://www.bakeryswap.org/#/home
- https://www.thetatoken.org/
- https://axieinfinity.com/
- https://www.chiliz.com/en/
- https://www.chiliz.com/en/
- https://audius.co/
- https://www.ecomi.com/
- https://ultra.io/
- https://www.sandbox.game/en/
- https://www.originprotocol.com/es/
- https://aragon.org/

## Autenticacion de Wallets
- https://blocto.portto.io/en/
- https://tatum.io/

## Recursos de Minting
- https://www.venly.io/selling-nfts
- https://docs.mintbase.io/dev/getting-data
- https://es.enjin.io/docs/minting-tokens
- https://www.skyflow.com/
- https://medium.com/klaytn/
- five-ways-to-mint-nfts-on-klaytn-cd359c0ae2a0
- https://wax-io.medium.com/the-ultimate-guide-how-to-create-nfts-on-wax-bdf94fea64d0
- https://docs.avax.network/build/tutorials/smart-digital-assets/creating-a-nft-part-1

## SmartContrats
 
- https://eth-brownie.readthedocs.io/en/stable/
- https://infura.io/